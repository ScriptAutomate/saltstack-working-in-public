# Working in Public, Chapter 2: The structure of an open source project

Some open source projects are small, while others can be gigantic.

A project, such as a small Sphinx extension, can have contributors move rather quickly in making their changes within a small community of contributors. In contrast, projects like SaltStack's [Salt](), are massive undertakings with extensive [Continuous Integration (CI)](https://aws.amazon.com/devops/continuous-integration/) pipelines that run a sleu of tests against contributions. These tests must pass before they can be merged, helping lift quite a bit of burden from the shoulders of maintainers.

Larger, more complex projects also result in contributor guides that can take quite a few more preliminary steps before churning out a contribution. Smaller projects may not even have a contributor guide, usually a [CONTRIBUTOR.md](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/setting-guidelines-for-repository-contributors), or have only a minimal one, that then expects the contributor to take on what problems may crop up. If the steps are a small enough, they may just be included in the [README.md](https://github.com/matiassingers/awesome-readme) file.

- [Salt’s Contributor Guide](https://docs.saltstack.com/en/latest/topics/development/contributing.html)
- [Salt's README](https://github.com/saltstack/salt/blob/master/README.rst)

## How contributions are made

A _Request For Comments_ (RFC) can be required in larger projects for changes to functionality, processes, and new features to launch community discussions around the why and how.

Here are some example RFC formats:
- Salt: [Salt Enhancement Proposals (SEPs)](https://github.com/saltstack/salt-enhancement-proposals)
- Python: [Python Enhancement Proposals (PEPs)](https://www.python.org/dev/peps/pep-0001/#:~:text=PEP%20stands%20for%20Python%20Enhancement,a%20rationale%20for%20the%20feature.)
- Go: [Design Documents](https://github.com/golang/proposal)

The easiest way to contribute to projects, when it comes to _pull requests_ (GitHub term for proposed code) or _merge requests_ (GitLab term for the same thing), is with bugfixes and documentation updates. If there is an existing issue in a repository, the friction in a submission would be even less.

Introducing new features, functions, modules, etc. are complex changes that involve more friction due to conversations around implementation, and whether it meets additionally expected guidelines/standards within a project.

## Where interactions take place

User's will notice that _repository_ and _project_ may be used interchangably on both GitHub and GitLab (among other code-hosting sites). _Project_ refers to the broader community involved in the maintaining and evolution of code repositories. Though `salt` is SaltStack's biggest project, it is part of the SaltStack community which involves many other projects like:

- [SaltStack Formulas](https://github.com/saltstack-formulas): Formulas are pre-written Salt States. They are as open-ended as Salt States themselves and can be used for tasks such as installing a package, configuring, and starting a service, setting up users or permissions, and many other common tasks. All official Salt Formulas are found as separate projects in the **saltstack-formulas** organization on GitHub.
- [pop](https://gitlab.com/saltstack/pop/pop)
  - [pop-awesome](https://gitlab.com/saltstack/pop/pop-awesome): A curated list of awesome POP projects, plugins, and resources.
  - `pop` is a Python implementation of the [Plugin Oriented Programming (POP)](https://pop-book.readthedocs.io/en/latest/) paradigm.
- [Idem](https://idem.readthedocs.io/en/latest/)
- [Umbra](https://umbra.readthedocs.io/en/latest/)

All of these projects are part of the same SaltStack community, which may often be referred to as the Salt community because of that project being the start of it all.

Norms around community engagement can differ between communities and projects. For SaltStack, there are several avenues for users and contributors to communicate with each other and hear from maintainers:

- [Salt Project Community Slack](https://saltstackcommunity.herokuapp.com/)
- [Salt Project IRC on Freenode](https://webchat.freenode.net/#salt)
- [Salt Project YouTube channel](https://www.youtube.com/channel/UCpveTIucFx9ljGelW63-BWg)
- [Salt Project Twitch channel](https://www.twitch.tv/saltstackinc)
- [Salt Project Email List](http://groups.google.com/group/salt-users)
- [Salt Community Events Calendar](https://calendar.google.com/calendar/embed?src=saltstack.com_md73c3ufcs2eqbsmmnike4em80%40group.calendar.google.com&ctz=America%2FNew_York)
- [Reddit: r/saltstack](https://www.reddit.com/r/saltstack/)
- [Salt StackOverflow](https://stackoverflow.com/questions/tagged/salt-stack%20or%20salt-cloud%20or%20salt-creation%20or%20salt-contrib?sort=Newest&edited=true)

### Issues and Pull Requests

Issues, also called _tickets_, generally fall into three categories:

- Bugs
- Features

For `salt`, we also have _Documentation Feedback_. This can be a problem of out-dated/incorrect documentation, missing documentation, a request for additional documentation such as more examples, etc.

For _Questions_, we provide links to some areas of the community. This is what a community member is shown when wanting to open a new issue:

- ["New issue" experience in `salt`](https://github.com/saltstack/salt/issues/new/choose)
  - [Based off of configurations found here](https://github.com/saltstack/salt/tree/master/.github/ISSUE_TEMPLATE) in the `salt` repository
  - GitHub Docs: [Configuring issue templates for your repository](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/configuring-issue-templates-for-your-repository)

## How projects change over time

> _I have dedicated my mind for the past little while to something that I have developed very high hopes for, something that I call -Salt._
>
> _...Before you get excited about salt development, it is still under heavy development (read – “it no worky yet”), but it is getting close, the message passing framework is complete, and the encryption and authentication are also functional. So it is coming along, and I expect the first alpha release to be available in the next few weeks. If you want to follow salt production (and you should!) check out the repo on github._
>
> Tom Hatch
> Technical Founder and CTO of SaltStack
> From [_Salt, we use it everywhere_](https://red45.wordpress.com/2011/03/06/salt-we-use-it-everywhere/)
> March, 2011

This was the first post by Tom, in March of 2011, about a project he called **Salt**. It was made available online, as an open source project under the Apache 2.0 license, with the first release being on March 19th, 2011: Version 0.6.0.

> TODO: Did Tom do anything else around the initial releases of Salt, such as announce it via other sites, user groups, conferences, online videos, etc?

### Evangelism

> TODO: What were the early stages of evangelism for Salt and SaltStack? Outside of Tom's personal blog in 2011 - 2013, what other avenues were taken to spread the word? Guest blogs, user groups, conferences, interviews, etc?

As 2012 was passing by, the project had more than 100 unique contributors, landing a mention in [GitHub's 2012 retrospective](https://github.blog/2012-12-19-the-octoverse-in-2012/).

### Growth

[When watching the timelapse of the Salt codebase](https://www.youtube.com/watch?v=Znmk8LO0fcE), from 2011 to 2014, you can see Tom being the initial contributor followed by the constant growth of a community. This video is missing what happens over the next six years: constant, exponential growth. This was creating some problems, which eventually led to some new projects: POP, Idem, Heist, and Umbra.

> _Linus Torvalds, Walt Disney, Richard Stallman, Guido Van Rossum, Steve Jobs and Bill Gates. These are just a few of the brilliant, innovative geniuses who became so good at what they did, they were no longer able to practice their craft._
> 
> _Walt Disney loved to draw. His passion is what created one of the most successful entertainment companies on the planet. However, he became so busy managing and maintaining his creation that he simply couldn’t afford to animate his own characters. Similarly, how much coding do you think Guido Van Rossum actually does today? For innovators, the unfortunate reality is that most (looking at you engineers, developers and content creators) will eventually become victims of their own creation._
>
> _Linus Torvalds learned: it’s all about the plugins, or some capability like plugins. This is why I created Plugin Oriented Programming for Infrastructure Automation, aka – POP._
>
> Tom Hatch
> Technical Founder and CTO of SaltStack
> From [_Pop Culture: Plugin Oriented Programming for Infrastructure Automation_](https://www.saltstack.com/blog/pop-culture-plugin-oriented-programming-for-infrastructure-automation/)
> January, 2020

### Classifying Project Types

TODO

